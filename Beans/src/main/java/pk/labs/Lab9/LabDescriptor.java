package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = Term.class;
    public static Class<? extends Consultation> consultationBean = Consultation.class;
    public static Class<? extends ConsultationList> consultationListBean = ConsultationList.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = ConsultationListFactory.class;
    
}
